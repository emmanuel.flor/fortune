# -*- coding: utf-8 -*-


db.define_table('fortune_log',
				Field('glory_id','integer'),
				Field('activity','text'),
				Field('date_created','datetime',default=request.now, update=request.now, writable=False),
				redefine=True
				),

db.define_table('fortune_user',
				Field('name','text'),
				Field('glory_id','integer'),
				Field('date_of_birth','string'),
				Field('photo','text'),
				Field('date_created','datetime',default=request.now, update=request.now, writable=False),
				redefine=True
				)